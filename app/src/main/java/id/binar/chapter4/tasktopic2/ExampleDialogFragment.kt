package id.binar.chapter4.tasktopic2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import id.binar.chapter4.tasktopic2.databinding.FragmentExampleDialogBinding

class ExampleDialogFragment : DialogFragment() {

    private var _binding: FragmentExampleDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentExampleDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeDialog()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun closeDialog() = binding.btnClose.setOnClickListener {
        Toast.makeText(requireContext(), "Dialog Fragment di Tutup", Toast.LENGTH_SHORT).show()
        dialog?.dismiss()
    }
}