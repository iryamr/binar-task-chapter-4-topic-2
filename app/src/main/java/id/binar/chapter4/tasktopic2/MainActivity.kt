package id.binar.chapter4.tasktopic2

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import id.binar.chapter4.tasktopic2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        showAlertDialogStandard()
        showAlertDialogWithAction()
        showCustomAlertDialog()
        showDialogFragment()
    }

    private fun showAlertDialogStandard() =
        binding.btnAlertDialogStandard.setOnClickListener { setAlertDialogStandard() }

    private fun setAlertDialogStandard() {
        AlertDialog.Builder(this).also {
            it.setTitle("Alert Dialog Standard")
            it.setMessage("Ini adalah Alert Dialog Standard")
            it.show()
        }
    }

    private fun showAlertDialogWithAction() =
        binding.btnAlertDialogWithAction.setOnClickListener { setAlertDialogWithAction() }

    private fun setAlertDialogWithAction() {
        AlertDialog.Builder(this).also {
            it.setTitle("Alert Dialog dengan Aksi")
            it.setMessage("Ini adalah Alert Dialog dengan Aksi")
            it.setPositiveButton("Positif") { _, _ -> showToast("Ini aksi positif") }
            it.setNegativeButton("Negatif") { _, _ -> showToast("Ini aksi negatif") }
            it.setNeutralButton("Netral") { _, _ -> showToast("Ini aksi netral") }
            it.setCancelable(false)
            it.show()
        }
    }

    private fun showCustomAlertDialog() =
        binding.btnCustomAlertDialog.setOnClickListener { setCustomAlertDialog(this) }

    @SuppressLint("InflateParams")
    private fun setCustomAlertDialog(context: Context) {
        val view = LayoutInflater.from(context).inflate(R.layout.custom_dialog, null, false)
        val builder = AlertDialog.Builder(context)
        builder.setView(view)

        val dialog = builder.create()
        val btnClose = view.findViewById<MaterialButton>(R.id.btn_close)

        btnClose.setOnClickListener {
            showToast("Custom Dialog di Tutup")
            dialog.dismiss()
        }

        dialog.setCancelable(false)
        dialog.show()
    }

    private fun showDialogFragment() =
        binding.btnDialogFragment.setOnClickListener { setDialogFragment() }

    private fun setDialogFragment() {
        val dialog = ExampleDialogFragment()
        dialog.isCancelable = false
        dialog.show(supportFragmentManager, null)
    }

    private fun showToast(message: String) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}